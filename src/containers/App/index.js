import React, { Component } from 'react';

import logo from '../../images/logo.svg';
import './App.css';

import {
  getMatrix,
  STRINGS,
  COLUMNS,
  MATRIX_SIZE,
  getNoisyArray,
  compareArray,
  getRandomInt,
  SAVED_OBJECTS_IN_LS,
  arrayToMatrix,
  testing,
  recognition
} from '../../object-recognition';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputMatrix: getMatrix(),
      ideal: [],
      noisyArray: [],

      object: '',

      testing: {}
    };
  }

  handleCellClick = (string, column, event) => {
    let matrix = this.state.inputMatrix;
    let cell = matrix[string][column];

    if (cell === 0) matrix[string][column] = 1;
    else matrix[string][column] = 0;

    this.setState({
      inputMatrix: matrix
    });
  };

  handleSaveClick = () => {
    let arr = [];

    for (let i = 0; i < STRINGS; i++) {
      for (let j = 0; j < COLUMNS; j++) {
        arr.push(this.state.inputMatrix[i][j]);
      }
    }

    let savedObjects = JSON.parse(localStorage.getItem(SAVED_OBJECTS_IN_LS));
    if (savedObjects === null) savedObjects = [];

    const OBJECT_NAME = `Object #${savedObjects.length / 10 + 1}`;
    const NOISY_ARRAY = getNoisyArray(arr);

    for (let element of NOISY_ARRAY) {
      let obj = {
        name: OBJECT_NAME,
        value: element
      };
      savedObjects.push(obj);
    }

    for (let i = 0; i < savedObjects.length * 10; i++) {
      const randomInt = getRandomInt(0, savedObjects.length);
      let buffer = savedObjects[0];
      savedObjects[0] = savedObjects[randomInt];
      savedObjects[randomInt] = buffer;
    }

    localStorage.setItem(SAVED_OBJECTS_IN_LS, JSON.stringify(savedObjects));
    console.log('savedObjects', savedObjects);

    this.setState({
      ideal: arr,
      noisyArray: NOISY_ARRAY
    });

    // alert('Объект сохранён');
  };

  handleRecognition = event => {
    let savedObjects = JSON.parse(localStorage.getItem(SAVED_OBJECTS_IN_LS));
    if (savedObjects === null) {
      savedObjects = [];

      this.setState({
        object: 'В хранилище отсутствуют образы'
      });
    } else {
      let arr = [];

      for (let i = 0; i < STRINGS; i++) {
        for (let j = 0; j < COLUMNS; j++) {
          arr.push(this.state.inputMatrix[i][j]);
        }
      }

      this.setState({
        object: recognition(arr)
      });
    }
  };

  handleSelect = (array, event) => {
    let matrix = [];

    for (let i = 0; i < MATRIX_SIZE; i += COLUMNS) {
      matrix.push(array.slice(i, COLUMNS + i));
    }

    this.setState({
      ideal: array,
      inputMatrix: matrix
    });
  };

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Object recognition</h2>
        </div>
        {this.state.ideal.length !== 0 && (
          <div className="Ideal">
            <h4>Сохранённый массив:</h4> {this.state.ideal}
          </div>
        )}
        <div className="Container">
          <div className="Grid">
            {this.state.inputMatrix.map((string, index) => {
              return (
                <div className="String" key={index}>
                  {string.map((cell, idx) => {
                    return (
                      <div
                        id={index + ':' + idx}
                        className="Cell"
                        key={index + idx}
                        onClick={this.handleCellClick.bind(this, index, idx)}
                        style={{
                          backgroundColor:
                            this.state.inputMatrix[index][idx] === 0
                              ? 'white'
                              : 'black'
                        }}
                      />
                    );
                  })}
                </div>
              );
            })}
          </div>
          <div className="NoisyInfo">
            {this.state.ideal.length !== 0 && (
              <div className="NoisyArray">
                {/* <h4>Массив шумов:</h4> */}
                {this.state.noisyArray.map((string, index) => {
                  return (
                    <div
                      className={
                        compareArray(this.state.ideal, string) ? (
                          'NoisyItem Active'
                        ) : (
                          'NoisyItem'
                        )
                      }
                      key={index}
                      onClick={this.handleSelect.bind(this, string)}
                    >
                      {string}
                    </div>
                  );
                })}
              </div>
            )}
          </div>
        </div>

        <div className="SaveButtonBlock">
          <button
            onClick={() => {
              this.setState({
                inputMatrix: getMatrix(),
                ideal: [],
                noisyArray: []
              });
            }}
          >
            Очистить поле
          </button>
          <button onClick={this.handleSaveClick}>Сохранить образ</button>
          {<button onClick={this.handleRecognition}>Распознать образ</button>}
        </div>
        {this.state.object.message !== undefined && (
          <div className="ObjectName">{this.state.object.message}</div>
        )}
        {this.state.object.value !== undefined && (
          <div className="ObjectName">
            <div>
              <h3>{this.state.object.name}</h3>
              {arrayToMatrix(this.state.object.value).map((string, index) => {
                return (
                  <div key={Math.random() + index} className="ResultString">
                    {string.map((cell, idx) => {
                      return (
                        <div
                          key={Math.random() + idx}
                          className={cell === 1 ? 'Result One' : 'Result Zero'}
                        />
                      );
                    })}
                  </div>
                );
              })}
            </div>
          </div>
        )}
        <div className="SaveButtonBlock">
          <button
            onClick={() => {
              localStorage.clear();
              alert('Хранилище очищено');
            }}
          >
            Очистить хранилище
          </button>
          <button onClick={() => this.setState({ testing: testing() })}>
            Провести тестирование
          </button>
        </div>
        {this.state.testing.average !== undefined && (
          <div className="Testing">
            <h3>Результаты тестирования</h3>
            <div className="Arr">
              {this.state.testing.arr.map((item, index) => {
                return (
                  <div key={index}>{`${Number(100 * item).toFixed(2)}%`}</div>
                );
              })}
            </div>
            <div>{`Среднее ${this.state.testing.average}%`}</div>
            <div>{`Медиана ${this.state.testing.mediana}%`}</div>
            <div>{`Разброс ${this.state.testing.scatter}%`}</div>
          </div>
        )}
      </div>
    );
  }
}

export default App;
