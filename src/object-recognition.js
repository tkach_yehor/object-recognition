export const STRINGS = 10;
export const COLUMNS = 8;

export const MATRIX_SIZE = STRINGS * COLUMNS;
export const PERCENT = 0.8;
export const RECOGNITION_PERCENT = 0.8;
export const NOISE_SIZE = MATRIX_SIZE * 0.15;

export const NOISE_ARRAY_COUNT = 10;

export const DEFAULT_VALUE = 0;

export const SAVED_OBJECTS_IN_LS = 'SAVED_OBJECTS_IN_LS';

export const getMatrix = () => {
  let inputMatrix = [];

  for (let i = 0; i < STRINGS; i++) {
    inputMatrix[i] = [];
    for (let j = 0; j < COLUMNS; j++) {
      inputMatrix[i][j] = DEFAULT_VALUE;
    }
  }

  return inputMatrix;
};

export const arrayToMatrix = arr => {
  let result = [];
  let index = 0;

  for (let i = 0; i < STRINGS; i++) {
    result[i] = [];
    for (let j = 0; j < COLUMNS; j++) {
      result[i][j] = arr[index];
      index++;
    }
  }

  // console.log('array to matrix')
  // console.log(result)

  return result;
};

export const testing = () => {
  let savedObjects = JSON.parse(localStorage.getItem(SAVED_OBJECTS_IN_LS));

  if (savedObjects && savedObjects.length > 0) {
    const LENGTH = savedObjects.length;
    const HALF = LENGTH / 2;

    let globalResult = [];
    let globalPercent = [];

    for (let iteration = 0; iteration < 15; iteration++) {
      let randomIndex = 0;
      while (randomIndex < 10) randomIndex = getRandomInt(10, LENGTH);

      let searchingObjects = [];
      let storage = [];

      if (randomIndex === HALF) {
        iteration--;
        continue;
      }

      if (randomIndex < HALF) {
        searchingObjects = savedObjects.slice(0, randomIndex);
        storage = savedObjects.slice(randomIndex, LENGTH);
      } else {
        searchingObjects = savedObjects.slice(randomIndex, LENGTH);
        storage = savedObjects.slice(0, randomIndex);
      }

      // console.log(iteration, searchingObjects.length)
      // console.log(iteration, storage.length)

      let result = [];
      for (let obj of searchingObjects) {
        result.push(recognition(obj.value, storage));
      }
      // console.log(result)
      globalResult.push(result);

      let count = 0;
      for (let item of result) {
        if (item.name !== undefined) count++;
      }

      globalPercent.push(Number((count / searchingObjects.length).toFixed(4)));
    }

    globalPercent.sort((a, b) => a - b);

    const MEDIANA = globalPercent[Math.floor(globalPercent.length / 2)];

    let sum = 0;
    for (let item of globalPercent) {
      sum += item;
    }

    const AVERAGE = sum / globalPercent.length;

    const SCATTER = globalPercent[globalPercent.length - 1] - globalPercent[0];

    // console.log(globalResult)
    console.log(globalPercent);

    console.log(AVERAGE);
    console.log(MEDIANA);
    console.log(SCATTER);

    return {
      arr: globalPercent,
      average: Number((100 * AVERAGE.toFixed(4)).toFixed(4)),
      mediana: Number((100 * MEDIANA.toFixed(4)).toFixed(4)),
      scatter: Number((100 * SCATTER.toFixed(4)).toFixed(4))
    };
  } else {
    return {};
  }
};

export const recognition = (input, storage) => {
  let savedObjects = JSON.parse(localStorage.getItem(SAVED_OBJECTS_IN_LS));

  if (storage !== undefined) {
    savedObjects = storage;
  }

  if (savedObjects.length === 0) {
    return {
      message: 'Хранилище объектов пустое'
    };
  }

  let maxCount = 0;
  let maxIndex = -1;

  for (let i = 0; i < savedObjects.length; i++) {
    let count = 0;
    for (let j = 0; j < savedObjects[i].value.length; j++) {
      if (input[j] === savedObjects[i].value[j]) {
        count++;
      }
    }
    if (count > maxCount) {
      maxCount = count;
      maxIndex = i;
    }
  }

  if (maxIndex === -1) {
    return {
      message: 'Объект не распознан'
    };
  }

  if (maxCount < MATRIX_SIZE * RECOGNITION_PERCENT) {
    return {
      message: 'Объект не распознан'
    };
  }

  // console.log('recognited object', savedObjects[maxIndex])

  return savedObjects[maxIndex];
};

export function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

export const getNoisyArray = ideal => {
  let noisyArray = [];
  // noisyArray.push(ideal);

  for (let i = 0; i < NOISE_ARRAY_COUNT; i++) {
    let buffer = ideal.slice(0, MATRIX_SIZE);

    let element;
    for (let j = 0; j < NOISE_SIZE; j++) {
      const RANDOM = getRandomInt(0, MATRIX_SIZE);

      element = buffer[RANDOM];
      if (element === 0) buffer[RANDOM] = 1;
      else {
        buffer[RANDOM] = 0;
        // j--
      }
    }

    let isExist = false;
    for (let j = 0; j < noisyArray.length; j++) {
      if (compareArray(buffer, noisyArray[j])) {
        i--;
        isExist = true;
      }
    }

    if (!isExist) noisyArray.push(buffer);
  }

  return noisyArray;
};

export const compareArray = (first, second) => {
  if (first.length !== second.length) return false;

  for (let i = 0; i < first.length; i++) {
    if (first[i] !== second[i]) return false;
  }

  return true;
};
